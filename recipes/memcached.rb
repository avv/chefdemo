#
# Cookbook:: chefdemo
# Recipe:: memcached
#
# Copyright:: 2017, The Authors, All Rights Reserved.

memcached_instance node['chefdemo']['memcached_name'] do
  port node['chefdemo']['memcached_port']
  memory node['chefdemo']['memcached_memory']
end
