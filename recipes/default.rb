#
# Cookbook:: chefdemo
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

%w[
  iptables
  nginx
  nodeapp
  memcached
  mysql
].each do |mod|
  include_recipe "chefdemo::#{mod}"
end
