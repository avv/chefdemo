#
# Cookbook:: chefdemo
# Recipe:: mysql
#
# Copyright:: 2017, The Authors, All Rights Reserved.

yum_repository 'mysql56-community' do
  name 'mysql56-community'
  baseurl 'https://repo.mysql.com/yum/mysql-5.6-community/el/$releasever/$basearch/'
  description ''
  enabled true
  gpgcheck false
end

mysql_service node['chefdemo']['mysql_name'] do
  port node['chefdemo']['mysql_port']
  initial_root_password node['chefdemo']['mysql_rootpw']
  action %I[create start]
end
