#
# Cookbook:: chefdemo
# Recipe:: nginx
#
# Copyright:: 2017, The Authors, All Rights Reserved.

node.set['nginx']['default_site_enabled'] = false

include_recipe 'chef_nginx'

nginx_site node['chefdemo']['nginx_name'] do
  template 'nginx_approxy.conf.erb'
end
