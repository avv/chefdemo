#
# Cookbook:: chefdemo
# Recipe:: iptables
#
# Copyright:: 2017, The Authors, All Rights Reserved.

include_recipe 'iptables::default'

iptables_rule 'iptables_http' do
  action :enable
end
