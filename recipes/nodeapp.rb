#
# Cookbook:: chefdemo
# Recipe:: nodeapp
#
# Copyright:: 2017, The Authors, All Rights Reserved.

include_recipe 'nodejs'
include_recipe 'nodejs::npm'

directory node['chefdemo']['nodeapp_directory'] do
  owner node['chefdemo']['nodeapp_owner']
  group node['chefdemo']['nodeapp_group']
  mode '0755'
  action :create
end

nodejs_npm 'express' do
  path node['chefdemo']['nodeapp_directory']
end

cookbook_file "#{node['chefdemo']['nodeapp_directory']}/app.js" do
  source 'app.js'
  owner node['chefdemo']['nodeapp_owner']
  group node['chefdemo']['nodeapp_group']
  mode '0644'
  action :create
end

cookbook_file "#{node['chefdemo']['nodeapp_directory']}/package.json" do
  source 'package.json'
  owner node['chefdemo']['nodeapp_owner']
  group node['chefdemo']['nodeapp_group']
  mode '0644'
  action :create
end

template '/lib/systemd/system/nodeapp.service' do
  source 'nodeapp.service.erb'
  notifies :stop, 'service[nodeapp]', :delayed
  notifies :start, 'service[nodeapp]', :delayed
end

service 'nodeapp' do
  provider Chef::Provider::Service::Systemd
  action :start
end
