const express = require('express')
const app = express()

app.set('port', process.env.NODE_PORT || 3000);

app.get('/', function (req, res) {
  res.send('Hello World!')
})

app.listen(app.get('port'), 'localhost', function () {
  console.log('Example app listening on port ' + app.get('port'))
})
