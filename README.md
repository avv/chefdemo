# Chefdemo

Chefdemo is a Chef cookbook that configures a local or remote server instance setting up the following:

* Nginx service in a reverse proxy mode
* Simple NodeJS app
* MemcacheD service
* MySQL server
* Iptables firewall configuration that restricts access to port 80 to traffic from 10.0.0.0/8 subnet or localhost

## Usage

To configure a local instance with chefdemo cookbook, run the following:

```
git clone https://bitbucket.com/avv/chefdemo
cd chefdemo
kitchen converge
```

Run basic tests using

```
kitchen verify
```

To destroy the local instance created with chefdemo, run

```
kitchen destroy
```

## Discussion Topics

*How would you make the destination port of the application configurable, so this could be used for other applications?*

Destination port of the application can be configured through node['chefdemo']['nodeapp_port'] attribute.

*How would you test the repeatability of this automation?*

Chefdemo cookbook includes the several tests in test/smote/default/default_test.rb. Test suite can be run through *kitchen verify*. More tests can be added if deemed necessary.

To insure repeatability of this automation, the cookbook should be added to the Continuous Integration system that periodically runs cookbook's test suite.


*How would you monitor the application and dependent services?*

In order to monitor the application and dependent services, application monitoring scripts need to be deployed as a part of configuration. Most modern monitoring frameworks such as Nagios/Icinga, Sensu, Consul, Telegraph, Collectd, and others, provide application monitoring templates.

Adding application monitoring scripts based on monitoring framework templates for nginx, memcached, mysql, and nodejs to the cookbook will be one of the first steps of monitoring configuration.

Some of the potential next steps of monitoring pipeline configuration include

* alerting configuration
* monitoring dashboard configuration
* logging configuration
* conducting failure mode effects analysis
