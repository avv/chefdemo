
default['chefdemo']['memcached_name'] = 'nodeapp'
default['chefdemo']['memcached_port'] = '11212'
default['chefdemo']['memcached_memory'] = '128'

default['chefdemo']['mysql_name'] = 'nodeapp'
default['chefdemo']['mysql_port'] = '3306'
default['chefdemo']['mysql_rootpw'] = 'changeme'

default['chefdemo']['nginx_name'] = 'nodeapp_proxy'
default['chefdemo']['nginx_port'] = '80'

default['chefdemo']['nodeapp_directory'] = '/application'
default['chefdemo']['nodeapp_owner'] = 'root'
default['chefdemo']['nodeapp_group'] = 'root'
default['chefdemo']['nodeapp_port'] = '3001'
