# # encoding: utf-8

# Inspec test for recipe chefdemo::default

describe iptables do
  it {
    should have_rule(
      '-A INPUT -s 10.0.0.0/8 -p tcp -m tcp --dport 80 -j ACCEPT'
    )
  }
end

describe iptables do
  it { should have_rule('-A INPUT -p tcp -m tcp --dport 80 -j DROP') }
end

describe service('mysql-nodeapp') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe service('memcached_nodeapp') do
  it { should be_installed }
  it { should be_running }
end

describe service('nodeapp') do
  it { should be_installed }
  it { should be_running }
end

describe port(3001) do
  it { should be_listening }
end

describe bash('curl -sS http://localhost') do
  its('stdout') { should match(/Hello World!/i) }
  its('stderr') { should eq '' }
end
